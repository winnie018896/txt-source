我們解除了阿尼瑪，聚集在結晶化了的赫洛斯・布立斯的周圍，觀望著釋放出淡淡光芒的它。

「真是不可思議呢，為甚麼會變成像這樣的石頭的呢」
「裡面的人還活著嗎─？」

芙蘭接近過去，想要觸碰它。
我慌忙靠近她，握住她的手勸告道。

「最好不要碰哦」
「誒，為甚麼？不就只是塊石頭嗎？」
「如果不小心被入侵到身體裡的話，腦袋就會變成滿是奧利哈魯鋼的事了」
「嗚誒─，那是甚麼啊。果然這個令人毛骨悚然啊」

芙蘭皺起了臉，遠離了那個結晶。

那麼，上次二話不說就把結晶直接破壊了，這次又該怎麼辦呢。
既然還發著光，所以我覺得他還沒死的，但是我不認為赫洛斯・布立斯還能從這個狀態中恢復過來。
話雖如此，關於奧利哈魯鋼的謎團有很多，為了慎重起見還是把它破壊掉比較好吧──

在我想著這樣的事情的時候，遠處傳來了咚、咚的步行聲。
往聲音傳來的方向回頭一看，那裡有率領著幾體阿尼瑪斯的，深藍色阿尼瑪的身影。
腰上配著一把大劍，雖然武器與雷斯雷庫提歐的不同，但這架阿尼瑪也儼然是正統派騎士的容貌。

「克里普托，你為甚麼來這裡了啊！」

然後，對於來到眼前的藍色阿尼瑪，基希尼亞露骨地對其惡言相向。
芙蘭也同樣鼓著臉頰。
克里普托──也就是說這傢伙是四將之一嗎。

「為甚麼？因為你擅自離開前線採取單獨行動，所以我才不得不追過來啊！？」
「啊啊？真敢說啊，那不是因為你把我家的芙蘭騙去王国了嗎」
「那只是戰力的有效利用罷了」
「真是站不住腳的藉口啊」

一觸即發的氣氛。
看向百合、艾爾萊亞和拉比，他們全都露出為難的表情。

⋯⋯不，艾爾萊亞並沒有。她只是笑嘻嘻地看著我而已。

這樣的話，能介入進行仲裁的就只有我嗎。
在這個情況下，四將這種怪物之間互相吵架也不好辦。

「好了好了，兩人都冷靜下來吧」
「局外人閉嘴！」
「有些事情是局外人才了解的哦，基希尼亞小姐。現在應該有比那件事更加重要的事情吧。那個該怎麼處理好呢」
「那個是⋯⋯啊啊，是啊。必須想辦法處理呢」
「哼⋯⋯野獸終於安定下來了嗎，這樣一來就可以理性地進行對話了」
「克里普托先生，是這樣叫的對吧？你也和基希尼亞小姐一樣沒有大人樣哦」
「咕，居然，說我和基希尼亞一樣！？」

或許被拿來比較的事令他感到相當屈辱吧，藍色的阿尼瑪握著拳頭表示不甘心。
嘛，會感到不甘心，即是說他多少也是有自覺的吧。

「哎—呀哎—呀，克里普托生—氣—了─！」
「哎—呀哎—呀！」

然後，看到他解除阿尼瑪變回肉身的狀態後，芙蘭和基希尼亞在煽風點火。
因為芙蘭還是個孩子、而且大概不能被看見，所以就算了，但我覺得基希尼亞果然是沒大人樣呢。

「哼，還是一如既往可惡的傢伙。那麼，你們是甚麼人？」

克里普托一邊盯著這邊一邊發問。
健碩的身體自不用說，深邃的面容，銳利的細眼，粗粗的眉毛。
還有被梳剪成短髮的深褐色頭髮。
年齡大概是３０歳多一點吧。
看上去是體育系的男性，威嚇起來會是相當可怕的。

「我叫岬・白詰。他們是我的伙伴，從右邊開始是百合・赤羽、艾爾萊亞・弗拉庫洛克、拉比・米澤拉。我們是在芙蘭西思的帶領下從王国過來的」
「⋯⋯岬・白詰嗎？」
「哎呀，那個名字難道是⋯⋯」

克里普托皺起眉頭。
基希尼亞也有反應，那果然四將剩下的最後一人是──

「好像是命經常誇耀的弟弟的名字吧」
「對啊」

⋯⋯誇耀，嗎。

面對突然聽到的喜訊，讓我感到胸口緊緊的。
我還以為肯定是被討厭著的。

「但眼前的岬・白詰是女的」
「也是呢，是同姓同名的別人嗎？」
「不，是本人哦。被召喚到這個世界的時候，由於不小心的失誤而被改變性別了」
「不小心的失誤能變成這樣的嗎⋯⋯啊，不，應該是有可能的吧。畢竟我是魔法的門外漢，而且特別是召喚魔法已經超出了我的理解範圍了」

既然召喚魔法這個詞語冒了出來，那看來姐姐似乎也是被同樣的方法帶到這個世界來的。
也就是說能使用那個魔法的魔法師，不是只有普拉娜絲而已嗎？
不過，如果被帶到帝国來的就只有姐姐一人，那麼那個規模就相當的小了。
普拉娜絲的優秀立刻突顯了出來。

「但是，就算是命的弟弟⋯⋯不，妹妹的話，也不能讓你直接跟她見面。還是說，從王国過來的目的就是為了見姐姐嗎？」
「不是那樣的，姐⋯⋯家姐在這裡的這件事，我是剛才聽到芙蘭西思提及才得知的。我來帝国的目的，是想加入軍隊」
「軍隊，嗎？」

克里普托瞇起本來就很細的眼睛細瞪著我。
不過說不定，他是因為眼神不好才這樣子看的而已。

「嘰嘻嘻嘻，這不是很好嗎。我可不會放跑這樣的優秀人才的，立即就讓你進入我的部隊裡吧！」
「等等，要把從王国過來的來歷不明的阿尼瑪使放進軍隊裡嗎？」
「不能信任嗎？但我覺得她比把自己人騙去王国的傢伙更值得信任，而且帝都有命在，也不用擔心她會背叛吧」
「不要把兩件事混為一談！」
「不，是一樣的哦」

由於平常關係就不好，所以談話進行得不怎順利。
不過，總覺得基希尼亞好像很感興趣的樣子。
她的部隊的名稱，記得好像是通稱為無法地帶【Lawless】的。
如果進入那裡取得一定戰果的話，或許就能按照計劃在帝国軍裡出人頭地了。

「克里普托還是一如既往的麻煩啊，明明騙了我，卻連一句對不起都沒有─」

芙蘭邊轉動腳尖捻著地面邊抱怨道。
這麼說來，克里普托的視線從剛才開始就好像一直沒有望向過她。

「不過，看不見所以也是沒辦法的呢」
「明明同樣都是四將，但卻看不見你嗎？」
「為了在萬一的時候可以攻擊他，基希尼亞這樣說並阻止我了」

我原以為只是水火不容而已，但沒想到居然甚至還考慮過要殺害。
看來克里普托和基希尼亞之間的隔閡比我想像的還深。
但是，現在總之，我必須得到克里普托的信任。
為了在軍隊中生存，只得到基希尼亞或芙蘭的中意是不行的。

「雖然不知道你會否相信我，但我是為了向王国復仇才來到帝国的」
「呵，復仇嗎」
「青梅竹馬被殺害，把這個罪名嫁禍給我了。我不能原諒這樣的他們。事實上，從王都來到這裡的期間，我已經摧毀了好幾個城鎮了」
「最近把蒙斯也給毀滅了哦」

百合補充說道。
克里普托用手抵住下巴，「把那個蒙斯給⋯⋯」地嘟囔著。
雖然，那是正因為有芙蘭的力量，所以才能做到的呢。

「嘛，如果有個萬一的話，讓芙蘭來嚓一聲地解決掉就好了吧，沒甚麼好擔心的哦。還是說你害怕自己被踢下四將的位置呢，克里普托少爺？」
「胡言亂語，看她的臉我就知道了。能力的差距是顯而易見的」
「你眼瞎呢」
「不要每句都咬著不放極力反駁我，你是髒兮兮的野狗嗎。好吧，就暫且先信任你吧。那麼，既然信任你了，那你能告訴我──在那裡的結晶是甚麼來的嗎？」

感覺，終於談到正題了。
克里普托用下巴指了指奧利哈魯鋼的結晶，並要求說明。
為了得到信任，此時只能公開情報了。

「名字是叫，奧利哈魯鋼。這是最近王国拿到了手的物質，具有使魔力增幅的力量」
「雖然在變成結晶之前，是一個使用了奧利哈魯鋼的阿尼瑪，但那簡直是怪物哦。我一個人根本不是對手」
「阿瓦麗提亞不是對手！？」

雖然不認可基希尼亞的人格，但還是承認了她的實力吧，克里普托第一次表現出感情，露出驚愕的樣子。

「也就是說，巴香堤被毀滅、地形被改變，都可以認為是那個奧利哈魯鋼的力量嗎？」
「雖然也有作為素材的機體原本就是優秀的阿尼瑪的緣故，但是有九成都是奧利哈魯鋼的力量的緣故吧」
「呼嗯，有必要進行調查呢。雷尼，你能叫一下運輸嗎？」
「好的，小事一樁，老大！」

在克里普托的背後，有一個像三色旗一樣的有三種顏色的花哨的阿尼瑪回話了。
男人的名字好像是叫雷尼。
和誇張的外表一樣，給人的印象也是很浮華造作的。

「都說了不要叫老大！」

克里普托雖然是個正經古板的人，但似乎也挺辛苦的。
部下輕浮，而同僚是基希尼亞和芙蘭。
很難掌控住的吧，我有點同情你了。

於是，雷尼從旁邊的阿尼瑪斯那裡拿來了像槍一樣的東西，朝著天空發射。
看來是信號彈。
發射的子彈在高空爆炸，放出藍色的光。

「那，是在幹甚麼呢」

百合歪著頭。
我也是，只知道那是在呼叫著甚麼而已。

「是在呼叫士康遞運輸吧」
「拉比君你知道啊」
「因為它很有名啊。它是包羅帝国全境運輸網的巨大公司，當發射了藍色的信號彈之後，不管在哪裡，都會出現搭乘著員工的阿尼瑪斯，來幫忙運送人和貨物」
「呵─，真方便呢。如果王国也有這種東西就好了」

在王国，說到運輸的話，是以馬車為主的。
雖然阿尼瑪斯的成本很高，但是速度遠遠比馬車快，而且動力也充足。
只要有足以投資設備的資金的話，用阿尼瑪斯的效率是高得多的。

「因為在雷古納托利科斯王国，利權都是密切糾纏在一起的，所以不會那麼簡單的」
「那個国家雖然很死板，但是權力很弱，所以多餘又浪費的障礙物太多了」

克里普托和基希尼亞回答了我的疑問。
確實，我切身感受到，卡普托的貧民窟和蒙斯的階級差異，都是有利於權力者的構造。
士康遞運輸之所以能夠出現在帝国，大概是因為這個国家重視自由的緣故吧。
但作為代價，帝国的治安似乎不太好。

被信號彈呼叫過來的士康遞運輸的阿尼瑪斯，大概５分鐘後就出現了。
全身都染成粉紅色的阿尼瑪斯，在胸口上大大地刻有士康遞運輸的商標。
一眼就能明白，連確認也沒必要做。
不過，他一來到我們的面前，就大聲地喊出了名號。

「感謝大家每一次的惠顧，這是３０年來都受到大家的喜愛，現在正在舉辦３０周年紀念活動的士康遞運輸！」

那是響徹周圍，充滿活力的聲音。

「要是沒有那個低級趣味的顏色的話，就１００分滿分了」
「那是那個社長的喜好，別說三道四了」
「有點像百合的伊莉提姆的顏色呢⋯⋯」
「艾爾萊亞你在說甚麼啊！一點都不像，絶對不像啊！」

在各人隨意地說著自己的感想時，一名男人從粉紅色的阿尼瑪斯上下來了。
阿尼瑪斯的種類是亞吉特，而且似乎是特別訂製成運送用的，它的背後安裝了很多貨架。
一想到這種阿尼瑪斯被滿滿地配置於能看見信號彈的範圍內，就覺得這間公司的規模實在是大得驚人。

「哎呀，這不是克里普托先生嗎，而且基希尼亞小姐也在！這可是意想不到的大型案件呢」
「和往常一樣，這是帝国方的委託。另外，請盡量不要把消息泄露出去」
「泄露出去的話我可是會被社長殺死的」
「哼，那也是呢。我也不想跟士康遞吵架」

克里普托浮現出苦笑。

「那麼，我希望能把這個結晶盡快運到帝都去」
「哇─，這是大物件啊。我可以叫同伴來嗎？一個人有點勉強呢」
「如果要叫同伴來的話，可以順便把我們也帶到帝都去嗎？」
「小事一件哦，老爺」

這樣說後，士康遞運輸的男人坐進了阿尼瑪斯，把腰上的信號彈射到了空中。
升空的子彈的顏色是紅色。
看來這個設計是，看見了那個的同伴們就會過來幫忙。

又過了１０分鐘左右。
兩架同樣是粉紅色的阿尼瑪斯聚集過來了。
雖然眼睛被熒光色閃得暈眩，但我們還是坐上了阿尼瑪斯帶在後面的車廂裡。
裡面相當寬敞，設置了像是電車裡那樣的長長的椅子，我們各自坐在自己喜歡的位置上。
當然，百合坐在我的旁邊，艾爾萊亞坐在我的膝蓋上。
然後芙蘭坐在不是百合坐著的我的另一個旁邊──而在最後，想要進來的基希尼亞，卻在入口處被叉著腿站立著的克里普托攔了下來。

「甚麼啊，難道你還想繼續吵架嗎？」
「你要回到戰場去啊，雖然我看不見，但是你已經確認到芙蘭西思平安無事了吧」
「一想到為了遷就一時之需伙伴又會被騙去王国的話，我就很不安啊」
「那就把她交給他們吧，芙蘭西思也很親近他們對吧？」
「⋯⋯嘛，雖然確實是那樣的」
「再者，如果基希尼亞你的部隊──無法地帶【Lawless】被放任不管的話，我會很頭疼的啊，光是你那邊的勤務兵是沒辦法統率他們的」
「交給茜拉果然還是不行嗎」
「不行的吧，我出來的時候已經超出極限了」

基希尼亞抱頭煩惱──最後她嘆了口氣，得出了結論。

「知—道啦，我現在就回去前線。吶，岬，我想讓你幫忙照顧芙蘭，可以拜託你嗎？」
「我會盡我所能的」
「這樣就足夠了。就是這樣，芙蘭，我走了」
「活著再見吧─！」
「彼此都是啊」

她輕輕地揮了揮手，就從車廂的入口離開了。
從窗戶往外面看，基希尼亞就站在稍稍離遠的地方。
看來她是要目送我們。

士康遞運輸的阿尼瑪斯開始移動，向帝都前進，基希尼亞的身影就漸漸遠離了。
芙蘭在直到看不見她為止，都一直揮著手。


════════════════════

註釋：

【１】「士康遞」，スキャンディー，拉丁語Scandi，意為攀爬、攀登、爬升。